from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id': '2'}, \
		 {'title': 'Python', 'id': '3'}]

@app.route('/book/JSON')
def bookJSON():
    return jsonify(books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template("showBook.html", books = books) #sending to html 
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook(): #add at the end 
    if request.method == "POST":
        newBook = request.form["name"] #from html file 
        newId = len(books) + 1 
        books.append({'title':newBook, "id":str(newId)})
        return redirect(url_for("showBook", books = books))
        
    else:
            return render_template("newBook.html")

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == "POST":
        newTitle = request.form["name"]
        for book in books:
            if book["id"] == str(book_id):
                book["title"] = newTitle
        return redirect(url_for("showBook", books = books))
    else:   
        return render_template("editBook.html", book_id = book_id, books = books)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == "POST":
        count = 0
        for book in books:
            if book["id"] == str(book_id) and int(book_id) == len(books): #is last book
                del books[count] # deletion in dict 
                break
            elif book["id"] == str(book_id):
                del books[count] 
                #need to edit all the id's after the change 
                for index in range(count, len(books)):
                    books[index]["id"] = str(int(books[index]["id"]) - 1)
                break
            count +=1
        return redirect(url_for("showBook", books = books))
    else:   
        return render_template("deleteBook.html", book_id = book_id, books = books)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5001)
	

